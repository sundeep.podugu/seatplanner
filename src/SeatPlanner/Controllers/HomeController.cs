﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SeatPlanner.Models;

namespace SeatPlanner.Controllers
{
    /// <summary>
    /// Default page - it is accessible only by the users authenticated by Azure AD
    /// </summary>
    [Authorize] 
    public class HomeController : Controller
    {
        private readonly string _connectionString;

        public HomeController(IConfiguration config)
        {
            // Retieves the connection string from the appsettings.json file
            _connectionString = config.GetConnectionString("SpacePlannerDbConnection");
        }

        /// <summary>
        /// Gets all the seats from the database
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult<List<Seat>>> Index()
        {
            var seats = await GetAll();
            return View(seats);
        }

        /// <summary>
        /// Books the seat for the user
        /// </summary>
        /// <param name="seatId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> BookSeat(int seatId)
        {
            // Gets the user name from Azure AD
            string username = User.Identity.Name;
            if (string.IsNullOrWhiteSpace(username))
            {
                return Json(new { success = false, message = "User name missing" });
            }

            // Checks if the seat is valid and is available in the database
            var seat = await GetSeat(seatId);
            if (seat == null)
            {
                return Json(new { success = false, message = "Seat not found" });
            }

            // Checks if the seat is already booked by another user in meantime
            if (seat.IsBooked)
            {
                return Json(new { success = false, message = "Seat already booked" });
            }

            // Books the seat for the user
            await BookSeat(seatId, username);

            return Json(new { success = true, message = "Booking successfull" });
        }

        /// <summary>
        /// If there are any errors in the application, it will redirect to this error page
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        /// <summary>
        /// Retrieves all seats from the database
        /// </summary>
        /// <returns></returns>
        private async Task<List<Seat>> GetAll()
        {
            List<Seat> seats = new List<Seat>();
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    // SQL query to get all the seats from the database
                    command.CommandText = "select seatid, floor, area, details, bookeduser, isbooked from seats";
                    command.CommandType = CommandType.Text;

                    // Open the SQL connection to read from the database
                    await connection.OpenAsync();

                    // Executes the data reader to read the rows returned from the query
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        // Loops over the rows returned
                        while (await reader.ReadAsync())
                        {
                            // Reads each row and populate into the seats collection
                            seats.Add(ReadSeat(reader));
                        }
                    }
                }
            }
            return seats;
        }

        /// <summary>
        /// Retrieves the seat based on the id
        /// </summary>
        /// <param name="seatId"></param>
        /// <returns></returns>
        private async Task<Seat> GetSeat(int seatId)
        {
            Seat seat = null;
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    // SQL query to a retrieve the seat matching the id
                    command.CommandText = "select seatid, floor, area, details, bookeduser, isbooked from seats where seatId = @seatId";
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue("@seatId", seatId);

                    // Open the SQL connection to read from the database
                    await connection.OpenAsync();

                    // Executes the data reader to read the rows returned from the query
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        // Checks if there are any matching records found
                        if (await reader.ReadAsync())
                        {
                            // Reads the record and populate the seat object
                            seat = ReadSeat(reader);
                        }
                    }
                }
            }
            return seat;
        }

        /// <summary>
        /// Books the seat for the user
        /// </summary>
        /// <param name="seatId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<int> BookSeat(int seatId, string user)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    // SQL query to update the seat record with the booking and user information
                    command.CommandText = "update seats set isbooked = 1, bookeduser = @bookedUser where seatId = @seatId";
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue("@seatId", seatId);
                    command.Parameters.AddWithValue("@bookedUser", user);

                    // Open the SQL connection to execute the update query
                    await connection.OpenAsync();

                    // Executs the SQL update command
                    return await command.ExecuteNonQueryAsync();
                }
            }
        }

        /// <summary>
        /// Read the values from the data reader and convert to seat object
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private Seat ReadSeat(IDataReader reader)
        {
            Seat seat = new Seat();

            seat.SeatId = reader.GetInt32(reader.GetOrdinal("seatid"));
            // Checks for database null values
            if (!reader.IsDBNull(reader.GetOrdinal("floor")))
            {
                // Assign the property based on the column
                seat.Floor = Convert.ToInt32(reader.GetOrdinal("floor"));
            }
            if (!reader.IsDBNull(reader.GetOrdinal("area")))
            {
                seat.Area = reader.GetString(reader.GetOrdinal("area"));
            }
            if (!reader.IsDBNull(reader.GetOrdinal("details")))
            {
                seat.Details = reader.GetString(reader.GetOrdinal("details"));
            }
            if (!reader.IsDBNull(reader.GetOrdinal("bookeduser")))
            {
                seat.BookedUser = reader.GetString(reader.GetOrdinal("bookeduser"));
            }
            if (!reader.IsDBNull(reader.GetOrdinal("isbooked")))
            {
                seat.IsBooked = reader.GetBoolean(reader.GetOrdinal("isbooked"));
            }

            return seat;
        }
    }
}
