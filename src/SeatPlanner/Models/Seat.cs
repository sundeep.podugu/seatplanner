﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeatPlanner.Models
{
    public class Seat
    {
        [Display(Name = "Seat ID")]
        public int SeatId { get; set; }

        [Display(Name = "Seat Floor")]
        public int Floor { get; set; }

        [Display(Name = "Seat Area")]
        public string Area { get; set; }

        [Display(Name = "Seat Details")]
        public string Details { get; set; }

        [Display(Name = "Booked By")]
        public string BookedUser { get; set; }

        [Display(Name = "Seat Status")]
        public bool IsBooked { get; set; }
    }
}
