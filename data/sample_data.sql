SET IDENTITY_INSERT [dbo].[Seats] ON 
GO
INSERT [dbo].[Seats] ([SeatId], [Floor], [Area], [Details], [BookedUser], [IsBooked]) VALUES (1, 1, N'NORTH', N'Identity Team', NULL, 0)
GO
INSERT [dbo].[Seats] ([SeatId], [Floor], [Area], [Details], [BookedUser], [IsBooked]) VALUES (2, 1, N'NORTH', N'Network Team', NULL, 0)
GO
INSERT [dbo].[Seats] ([SeatId], [Floor], [Area], [Details], [BookedUser], [IsBooked]) VALUES (3, 1, N'SOUTH', N'Network Team', NULL, 0)
GO
INSERT [dbo].[Seats] ([SeatId], [Floor], [Area], [Details], [BookedUser], [IsBooked]) VALUES (4, 2, N'NORTH', N'Domains Team', NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[Seats] OFF
GO
