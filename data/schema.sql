/****** Object:  Table [dbo].[Seats]    Script Date: 2019-05-24 9:28:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Seats](
	[SeatId] [int] IDENTITY(1,1) NOT NULL,
	[Floor] [int] NULL,
	[Area] [nvarchar](20) NULL,
	[Details] [nvarchar](50) NULL,
	[BookedUser] [nvarchar](256) NULL,
	[IsBooked] [bit] NOT NULL,
 CONSTRAINT [PK_Seats] PRIMARY KEY CLUSTERED 
(
	[SeatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Seats] ADD  CONSTRAINT [DF_Seats_IsBooked]  DEFAULT ((0)) FOR [IsBooked]
GO